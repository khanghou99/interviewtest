<h3>Question A1</h3>
<p>1. Wrote a unit test</p>
<p>2. Implemented as many common methods into the Animal class as possible, leaving only the sing function to the Bird class</p>
<p>3. Created a constructor for Bird that sets the default parameters such as canFly, canWalk and canSwim. I also separated the sing function's return value from the speak function's return value</p>


<h3>Question A2</h3>
<p>1. Added constructor to specify that a Duck should be able to swim as well as what it should say. I did this for every class afterwards, including the Chicken.
</p>


<h3>Question A3</h3>
<p>1. Implemented a Gender enum to distinguish between hens and roosters. Depending on their gender, they say two different things</p>


<h3>Question A4</h3>
<p>1. A parrot is special because its speech depends. To make it as simple as possible, I simply added another constructor that accepts a phrase as a parameter. The parrot will then repeat that phrase whenever speak() is called</p>


<h3>Question B1</h3>
<p>1. Wrote a unit test for the fish's speak function. When called, it should throw an UnsupportedOperationException</p>


<h3>Question B2</h3>
<p>1. Because of the question requirements, I added three new properites under Animal: color, size and hobby. Color and hobby being strings and size being an enum with 3 possible values: LARGE, MEDIUM and SMALL. Although these properties aren't used outside of the question, it allows for future code maintainability if the need to use color, size or hobby on any animal class ever arises</p>


<h3>Question B3</h3>
<p>1. I decided to create a Mammal class just for the Dolphin. I only set the canWalk property to false in the Dolphin constructor since it's parent, the Mammal class automatically sets canSwim to true</p>


<h3>Question D1</h3>
<p>1. I created an insect class to serve as the parent class for Butterfly</p>
<p>2. Also added a new boolean value in Animal class: quiet;</p>


<h3>Question D2</h3>
<p>1. Added a new enum: LifecycleStage with 4 possible values: EGG, LARVA, PUPA and ADULT</p>
<p>2. Butterfly now has another constructor that accepts a LifecycleStage enum. Its properties to walk and fly depends on the value passed. By default though, instancing a Butterfly without a constructor will result it a butterfly.</p>
<p>3. Wrote multiple unit tests for the butterfly's different stages</p>


<h3>Question E1</h3>
<p>1. Used stream API to obtain the required values. </p>
<p>1. Added a new property: canSing into Animal clss and modified the speak function. Removed bird's sing function and lyrics and put the sing function into the Animal class. Now the sing function will check to see if canSing is true and automatically prepends a "I am singing:" to the animal' speech otherwise it will return "I can't sing"</p>







