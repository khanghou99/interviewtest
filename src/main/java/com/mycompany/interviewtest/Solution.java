/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interviewtest;

import com.mycompany.interviewtest.animals.Animal;
import com.mycompany.interviewtest.animals.bird.Bird;
import com.mycompany.interviewtest.animals.bird.Chicken;
import com.mycompany.interviewtest.animals.bird.Duck;
import com.mycompany.interviewtest.animals.bird.Parrot;
import com.mycompany.interviewtest.animals.fish.Clownfish;
import com.mycompany.interviewtest.animals.fish.Fish;
import com.mycompany.interviewtest.animals.fish.Shark;
import com.mycompany.interviewtest.animals.insect.Butterfly;
import com.mycompany.interviewtest.animals.insect.LifecycleStage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author khanghou
 */
public class Solution {
    
    public static void main(String[] args) {
        Bird bird = new Bird();
        boolean birdCanWalk = bird.canWalk();
        boolean birdCanFly = bird.canFly();
        Duck duck = new Duck();
        Parrot parrot = new Parrot(duck.speak());
        System.out.println(duck.speak());
        System.out.println(parrot.speak());
        System.out.println(bird.speak());
        
        Animal[] animals = new Animal[]{
           new Bird(),
            new Duck(),
            new Chicken(),
            new Parrot(),
            new Fish(),
            new Shark(),
            new Clownfish(),
            new Butterfly()
        };
        
        List<Animal> animalsThatFly = Arrays.asList(animals).stream()
                .filter(a -> a.canFly() == true)
                .collect(Collectors.toList());
        
        List<Animal> animalsThatWalk = Arrays.asList(animals).stream()
                .filter(a -> a.canWalk() == true)
                .collect(Collectors.toList());
        
        List<Animal> animalsThatSwim = Arrays.asList(animals).stream()
                .filter(a -> a.canSwim() == true)
                .collect(Collectors.toList());
        
        List<Animal> animalsThatSing = Arrays.asList(animals).stream()
                .filter(a -> a.canSing() == true)
                .collect(Collectors.toList());
        
         System.out.println("Number of animals that walk: " + animalsThatWalk.size());
         System.out.println("Number of animals that swim: " + animalsThatSwim.size());
         System.out.println("Number of animals that fly: " + animalsThatFly.size());
         System.out.println("Number of animals that sing: " + animalsThatSing.size());
    }
}
