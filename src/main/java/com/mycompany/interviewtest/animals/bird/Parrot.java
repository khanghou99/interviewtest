/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interviewtest.animals.bird;

/**
 *
 * @author khanghou
 */
public class Parrot extends Bird{
    
    public Parrot(){
        this.speech= "Squaawk, I'm a parrot";
    }
    
    public Parrot(String speech){
       
        this.speech = speech;
    }
}
