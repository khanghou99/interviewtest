/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interviewtest.animals.bird;

import com.mycompany.interviewtest.enums.Gender;

/**
 *
 * @author khanghou
 */
public class Chicken extends Bird {
    
    public Chicken(){
        this.canFly = false;
        this.speech = "Cluck, cluck";    
    }
    
    public Chicken(Gender gender){ 
        this();
        this.gender = gender;
        if(gender == Gender.MALE){
            this.speech = "Cock-a-doodle-doo";
        } 
    }
}
