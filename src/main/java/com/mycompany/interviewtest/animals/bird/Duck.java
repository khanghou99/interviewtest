/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interviewtest.animals.bird;

/**
 *
 * @author khanghou
 */
public class Duck extends Bird {
    
    public Duck(){
        this.speech = "Quack, quack";
        this.canSwim = true;
    }
}
