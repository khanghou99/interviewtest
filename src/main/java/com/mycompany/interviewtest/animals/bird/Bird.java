/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interviewtest.animals.bird;

import com.mycompany.interviewtest.animals.Animal;

/**
 *
 * @author khanghou
 */
public class Bird extends Animal{
    protected String lyrics;
    public Bird(){
        this.canFly = true;    
        this.canWalk = true;
        this.canSing = true;
        this.canSwim = false;
        this.speech = "I am a bird!";
    }
}
