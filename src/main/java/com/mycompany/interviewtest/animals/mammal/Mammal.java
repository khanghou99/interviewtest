/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interviewtest.animals.mammal;

import com.mycompany.interviewtest.animals.Animal;

/**
 *
 * @author khanghou
 */
public class Mammal extends Animal{
    public Mammal(){
        this.canWalk = true;
        this.canSwim = true;
        this.canFly = false;
    }
}
