package com.mycompany.interviewtest.animals.mammal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author khanghou
 */
public class Dolphin extends Mammal{
    public Dolphin(){
        this.canWalk = false;
        this.canSing = true;
    }
}
