/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interviewtest.animals.insect;

import com.mycompany.interviewtest.animals.Animal;

/**
 *
 * @author khanghou
 */
public class Insect extends Animal{
    
    LifecycleStage lifecycleStage;
    public Insect(){
        this.canFly = true;
        this.canWalk = true;
        this.canSwim = false;
    }
}
