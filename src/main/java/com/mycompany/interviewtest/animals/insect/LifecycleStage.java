/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interviewtest.animals.insect;

/**
 *
 * @author khanghou
 */
public enum LifecycleStage {
    EGG,
    LARVA,
    PUPA,
    ADULT
}
