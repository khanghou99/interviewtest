/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interviewtest.animals.insect;

/**
 *
 * @author khanghou
 */
public class Butterfly extends Insect{
    
    public Butterfly(){
        this.canWalk = true;
        this.canFly = true;
        this.canSwim = false;
        this.quiet = true;
        this.lifecycleStage = LifecycleStage.ADULT;
    }
    
    public Butterfly(LifecycleStage lifecycleStage){
        this.lifecycleStage = lifecycleStage;
        switch(lifecycleStage){
            case EGG:
            case PUPA:
                this.canWalk = false;
                this.canSwim = false;
                this.canFly = false;
                break;
            case LARVA:
                this.canWalk = true;
                this.canSwim = false;
                this.canFly = false;
                break;
            default:
                this.canWalk = true;
                this.canFly = true;
                this.canSwim = false;
                break;
        }
    }
}
