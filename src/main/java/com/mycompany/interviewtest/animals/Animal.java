/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interviewtest.animals;

import com.mycompany.interviewtest.enums.Gender;
import com.mycompany.interviewtest.enums.Size;

/**
 *
 * @author khanghou
 */
public class Animal {
    
    protected boolean canWalk;
    protected boolean canSwim;
    protected boolean canFly;
    protected boolean canSing;
    protected String speech;
    protected Gender gender;
    protected Size size;
    protected String color;
    protected String hobby;
    protected boolean quiet;

    public boolean isQuiet() {
        return quiet;
    }
    
    public boolean canSing(){
        return canSing;
    }

    public boolean canWalk() {
        return canWalk;
    }

    public boolean canSwim() {
        return canSwim;
    }

    public boolean canFly() {
        return canFly;
    }

    public String speak() {
        return speech;
    }
    
    public String sing(){
        if(canSing){
            return "I am singing: " + speech;
        } else {
            return "I can't sing";
        }
    }

    public Gender getGender() {
        return gender;
    }

    

    public Size getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }

    public String getHobby() {
        return hobby;
    }
    
    
}
