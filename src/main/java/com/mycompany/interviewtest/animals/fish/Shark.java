/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interviewtest.animals.fish;

import com.mycompany.interviewtest.enums.Size;

/**
 *
 * @author khanghou
 */
public class Shark extends Fish{
    public Shark(){
        this.size = Size.LARGE;
        this.color = "grey";
        this.hobby = "Eating other fish";
    }
}
