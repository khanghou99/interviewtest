/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interviewtest.animals.fish;

import com.mycompany.interviewtest.animals.Animal;

/**
 *
 * @author khanghou
 */
public class Fish extends Animal{
    public Fish(){
        this.canWalk = false;
        this.canSwim = true;
        this.canFly= false;
    }
    
    @Override
    public String speak() throws UnsupportedOperationException{
        throw new UnsupportedOperationException("Fish don't speak");
    }
}
