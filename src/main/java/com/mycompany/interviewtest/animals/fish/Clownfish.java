/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interviewtest.animals.fish;

import com.mycompany.interviewtest.enums.Size;

/**
 *
 * @author khanghou
 */
public class Clownfish extends Fish{
    public Clownfish(){
        this.size = Size.SMALL;
        this.color = "orange";
        this.hobby = "Cracking jokes";
    }
}