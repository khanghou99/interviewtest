/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.fish;

import com.mycompany.interviewtest.animals.fish.Fish;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author khanghou
 */
public class FishTest {
    
    @Test
    public void fishShouldNotSpeak(){
        try{
            
            Fish fish = new Fish();
            fish.speak();
            Assert.fail("Should have thrown an exception");
        } catch (Exception ex){
            
            String expectedMessage = "Fish don't speak";
            Assert.assertEquals(expectedMessage, ex.getMessage());
        }
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
