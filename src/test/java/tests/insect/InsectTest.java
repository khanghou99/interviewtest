/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.insect;

import com.mycompany.interviewtest.animals.insect.Butterfly;
import com.mycompany.interviewtest.animals.insect.LifecycleStage;
import junit.framework.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author khanghou
 */
public class InsectTest {
    
    public InsectTest() {
    }

    @Test
    public void butterflyShouldFly(){
        Butterfly butterfly = new Butterfly();
        Assert.assertTrue(butterfly.canFly());
    }
        
    @Test
    public void larvaShouldNotFly(){
        Butterfly butterfly = new Butterfly(LifecycleStage.LARVA);
        Assert.assertFalse(butterfly.canFly());
    }
    
    @Test
    public void eggShouldNotWalk(){
        Butterfly butterfly = new Butterfly(LifecycleStage.EGG);
        Assert.assertFalse(butterfly.canWalk());
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
