/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.bird;

import com.mycompany.interviewtest.animals.bird.Chicken;
import com.mycompany.interviewtest.animals.bird.Parrot;
import com.mycompany.interviewtest.enums.Gender;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author khanghou
 */
public class ParrotTest {
    
    @Test
    public void parrotShouldCrowWhenNextToRooster(){
        Chicken rooster = new Chicken(Gender.MALE);
        Parrot parrot = new Parrot(rooster.speak());
        Assert.assertEquals(rooster.speak(), parrot.speak());
    }
    
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
