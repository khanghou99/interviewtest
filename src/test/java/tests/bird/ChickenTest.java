/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.bird;

import com.mycompany.interviewtest.animals.bird.Chicken;
import com.mycompany.interviewtest.enums.Gender;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author khanghou
 */
public class ChickenTest {
    
    @Test
    public void chickenShouldNotFly(){
        Chicken chicken = new Chicken();
        Assert.assertFalse(chicken.canFly());
    }
    
    @Test
    public void chickenShouldCluck(){
        Chicken chicken = new Chicken();
        String expectedSpeech = "Cluck, cluck";
        Assert.assertEquals(expectedSpeech, chicken.speak());
    }
    
    @Test
    public void roosterShouldCrow(){
        Chicken chicken = new Chicken(Gender.MALE);
        String expectedSpeech = "Cock-a-doodle-doo";
        Assert.assertEquals(expectedSpeech, chicken.speak());
        
    }
}
