/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.bird;

import com.mycompany.interviewtest.animals.bird.Bird;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author khanghou
 */
public class BirdTest {
    @Test
    public void birdShouldSing(){
        
        Bird bird = new Bird();
        
        Assert.assertTrue(bird.canSing());
    
    }
}
