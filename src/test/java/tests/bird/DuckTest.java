/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.bird;

import com.mycompany.interviewtest.animals.bird.Duck;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author khanghou
 */
public class DuckTest {
    
    @Test
    public void duckShouldSwim(){
        Duck duck = new Duck();
        Assert.assertTrue(duck.canSwim());
    }
    
    @Test
    public void duckShouldQuack(){
        
        Duck duck = new Duck();
        String expectedSpeech = "Quack, quack";
        Assert.assertEquals(expectedSpeech, duck.speak());
    }
}
